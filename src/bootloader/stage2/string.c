#include "string.h"
#include "stdint.h"

/**
 * @brief Does string contains a given char
 *
 * @param str
 * @param chr
 * @return const char*
 */
const char *strchr(const char *str, char chr) {
    if (str == NULL)
        return NULL;

    while (*str) {
        if (*str == chr)
            return str;

        ++str;
    }

    return NULL;
}

/**
 * @brief Copy a string
 *
 * @param dst
 * @param src
 * @return char*
 */
char *strcpy(char *dst, const char *src) {
    char *origDst = dst;

    if (dst == NULL)
        return NULL;

    if (src == NULL) {
        *dst = '\0';
        return dst;
    }

    while (*src) {
        *dst = *src;
        ++src;
        ++dst;
    }

    *dst = '\0';
    return origDst;
}

/**
 * @brief Compute the string length
 *
 * @param str
 * @return unsigned
 */
unsigned strlen(const char *str) {
    unsigned len = 0;
    while (*str) {
        ++len;
        ++str;
    }

    return len;
}
