#include "ctype.h"

/**
 * @brief Is char lowercase
 *
 * @param chr
 * @return true
 * @return false
 */
bool islower(char chr) { return chr >= 'a' && chr <= 'z'; }

/**
 * @brief Set char uppercase
 *
 * @param chr
 * @return char
 */
char toupper(char chr) { return islower(chr) ? (chr - 'a' + 'A') : chr; }
