#include "utility.h"

/**
 * @brief Align two addresses
 *
 * @param number
 * @param alignTo
 * @return uint32_t
 */
uint32_t align(uint32_t number, uint32_t alignTo) {
    if (alignTo == 0)
        return number;

    uint32_t rem = number % alignTo;
    return (rem > 0) ? (number + alignTo - rem) : number;
}
