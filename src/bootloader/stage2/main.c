#include "disk.h"
#include "fat.h"
#include "stdint.h"
#include "stdio.h"

void far *g_data = (void far *)0x00500200;

void __cdecl cstart_(uint16_t bootDrive) {
    DISK disk;
    if (!DISK_Initialize(&disk, bootDrive)) {
        printf("Disk init error\r\n");
        goto end;
    }

    DISK_ReadSectors(&disk, 19, 1, g_data);

    if (!FAT_Initialize(&disk)) {
        printf("FAT init error\r\n");
        goto end;
    }

    // browse files in root
    FAT_File far *fd = FAT_Open(&disk, "/");
    FAT_DirectoryEntry entry;
    int i = 0;
    printf("\r\nReading from \"/\" :\r\n");
    while (FAT_ReadEntry(&disk, fd, &entry) && i++ < 5) {
        printf("  ");
        for (int i = 0; i < 11; i++)
            putc(entry.Name[i]);
        printf("\r\n");
    }
    FAT_Close(fd);

    // read test.txt
    fd = FAT_Open(&disk, "mydir/test.txt");
    char buffer[100];
    uint32_t read;
    printf("\r\nPrinting \"mydir/test.txt\" file content:\r\n");
    while ((read = FAT_Read(&disk, fd, sizeof(buffer), buffer))) {
        for (uint32_t i = 0; i < read; i++) {
            if (buffer[i] == '\n')
                putc('\r');
            putc(buffer[i]);
        }
    }
    FAT_Close(fd);

end:
    for (;;)
        ;
}
