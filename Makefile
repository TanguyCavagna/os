ASM=nasm
CC=gcc
CC16=/usr/bin/watcom/binl/wcc
LD16=/usr/bin/watcom/binl/wlink

SRC=src
TOOLS=tools
BUILD=build

.PHONY: all floppy_image kernel bootloader clean always tools_fat

all: floppy_image tools_fat

#
# Floppy image
#
floppy_image: $(BUILD)/main_floppy.img

$(BUILD)/main_floppy.img: bootloader kernel
	dd if=/dev/zero of=$(BUILD)/main_floppy.img bs=512 count=2880
	mkfs.fat -F 12 -n "AMOS" $(BUILD)/main_floppy.img
	dd if=$(BUILD)/stage1.bin of=$(BUILD)/main_floppy.img conv=notrunc
	mcopy -i $(BUILD)/main_floppy.img $(BUILD)/stage2.bin "::stage2.bin"
	mcopy -i $(BUILD)/main_floppy.img $(BUILD)/kernel.bin "::kernel.bin"
	mmd -i $(BUILD)/main_floppy.img "::mydir"
	mcopy -i $(BUILD)/main_floppy.img test.txt "::mydir/test.txt"

#
# Bootloader
#
bootloader: stage1 stage2

stage1: $(BUILD)/stage1.bin

$(BUILD)/stage1.bin: always
	$(MAKE) -C $(SRC)/bootloader/stage1 BUILD=$(abspath $(BUILD))

stage2: $(BUILD)/stage2.bin

$(BUILD)/stage2.bin: always
	$(MAKE) -C $(SRC)/bootloader/stage2 BUILD=$(abspath $(BUILD))

#
# Kernel
#
kernel: $(BUILD)/kernel.bin

$(BUILD)/kernel.bin: always
	$(MAKE) -C $(SRC)/kernel BUILD=$(abspath $(BUILD))

#
# Tools
#
tools_fat: $(BUILD)/tools/fat
$(BUILD)/tools/fat: always $(TOOLS)/fat/fat.c
	mkdir -p $(BUILD)/tools
	$(CC) -g -o $(BUILD)/tools/fat $(TOOLS)/fat/fat.c

#
# Always
#
always:
	mkdir -p $(BUILD)

#
# Clean
#
clean:
	$(MAKE) -C $(SRC)/bootloader/stage1 BUILD=$(abspath $(BUILD)) clean
	$(MAKE) -C $(SRC)/bootloader/stage2 BUILD=$(abspath $(BUILD)) clean
	$(MAKE) -C $(SRC)/kernel BUILD=$(abspath $(BUILD)) clean
	rm -rf $(BUILD)/*

#
# QEMU setup
#
run:
	qemu-system-i386 -fda build/main_floppy.img

debug:
	qemu-system-i386 -fda build/main_floppy.img -gdb tcp::1234 -S
